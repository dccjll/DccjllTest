package com.dccjll.test.threadsync;

public class SaleTickets implements Runnable{
	
	private int ticketCount = 100;// 总的票数
	Object mutex = new Object();// 锁
	
	/**
     * 卖票
     */
    public void sellTicket()
    {
        synchronized (mutex)// 当操作的是共享数据时,
                        // 用同步代码块进行包围起来,这样在执行时,只能有一个线程执行同步代码块里面的内容
        {
            if (ticketCount > 0)
            {
            	System.out.print("\n" + Thread.currentThread().getName() + "正在卖第" + ticketCount + "张票");
                ticketCount--;
                System.out.println("还剩" + ticketCount + "张票");
            }
            else
            {
                System.out.println("票已经卖完！");
                return;
            }
        }
    }

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (ticketCount > 0)// 循环是指线程不停的去卖票
        {
            sellTicket();
            /**
             * 在同步代码块里面睡觉,和不睡效果是一样 的,作用只是自已不执行,也不让线程执行
             * 所以把睡觉放到同步代码块的外面,这样卖完一张票就睡一会,让其他线程再卖,这样所有的线程都可以卖票
             */
            try
            {
                Thread.sleep(100);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
	}

}
