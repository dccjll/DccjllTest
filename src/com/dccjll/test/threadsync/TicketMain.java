package com.dccjll.test.threadsync;

public class TicketMain {
	public static void main(String[] args)
    {
        SaleTickets runTicekt = new SaleTickets();
        Thread th1 = new Thread(runTicekt, "窗口1");
        Thread th2 = new Thread(runTicekt, "窗口2");
        Thread th3 = new Thread(runTicekt, "窗口3");
        Thread th4 = new Thread(runTicekt, "窗口4");
        th1.start();
        th2.start();
        th3.start();
        th4.start();
    }
}
